// [SECTION] MongoDB Aggregation
/*
	- Use dto generate, manipulate, and perform operations to create filtered results that helps us to analyze the data.
*/

// Using the aggregate method:
/*
	- The "$match" is used to pass the document that meet the specified condition(s) to the next stage/aggregation process.
	- Syntax:
		{$match: {field: value}}
	- The "$group"
	- Syntax:
		{$group: {_id: "value", fieldResult: "valueResult"}
		}
*/

db.fruits.aggregate([
	{$match: {onSale: true}}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
]);

// Inclusion / Exclusion
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
]);

// Sorting Aggregated Results
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: 1}}
]);

// Aggregating result based on array fields
/*
	- The "$unwind" ...
*/
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", kinds: {$sum: 1}}},
]);

// [SECTION] Other Aggregate Stages
// $count all yellow fruits
db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$count: "Yellow Fruits"},
]);

// $avg gets the average value of stock
db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
]);

// $min $max
db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock: {$min: "$stock"}}}
]);

db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock: {$max: "$stock"}}}
]);